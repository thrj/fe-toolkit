One can read the FE-ToolKit documentation online at: https://rutgerslbsr.gitlab.io/fe-toolkit

This repository is a collection of programs useful for free energy analysis.
The programs are stored in individual tarballs. Each program has its own
license, which can be viewed by extracting the contents of the program
distribution tarball.

The programs included in this distribution include:


ndfes     - a program for evaluating free energy profiles from umbrella
            window simulations using either vFEP or MBAR analysis.

edgembar  - a program for performing networkwide free energy analysis
            of alchemical free energy transformation graphs typically
	    constructed to compute relative binding (or solvation)
	    free energies.

The programs rely on the presence of external software not included.
Specifically, these programs require an implementation of BLAS, LAPACK,
and NLopt (https://github.com/stevengj/nlopt). Furthermore, the
installation is based on the GNU autoconf/automake tools. If you need
to install NLopt from source, then you'll also need to install cmake.
A version of python >= 3.6 will be required.

If you are using Fedora, you can install the missing dependencies with:
```
   sudo dnf install autoconf.noarch automake.noarch cmake.x86_64 \
                    openblas-serial.x86_64 NLopt.x86_64 NLopt-devel.x86_64 \
                    python3-numpy.x86_64 python3-scipy.x86_64 \
		    python3-matplotlib.x86_64
```

To install a program into PREFIX, you can run:
- tar -xzf program.tar.gz
- cd program
- ./configure --with-openmp CXX="gcc" CXXFLAGS="-O2 -g" --prefix=${PREFIX}
- PYTHONUSERBASE=\${PREFIX} python3 -m pip install --prefix=\${PREFIX} src/python


We request that if you use this software in a publication, to please reference
as appropriate:

[1] Alchemical Binding Free Energy Calculations in AMBER20: Advances and Best 
Practices for Drug Discovery
Tai-Sung Lee, Bryce K. Allen, Timothy J. Giese, Zhenyu Guo, Pengfei Li, 
Charles Lin, T. Dwight McGee, David A. Pearlman, Brian K. Radak, Yujun Tao, 
Hsu-Chun Tsai, Huafeng Xu, Woody Sherman, Darrin M. York
J. Chem. Inf. Model. (2020) 60, 5595-5623
DOI: 10.1021/acs.jcim.0c00613

[2] Variational Method for Networkwide Analysis of Relative Ligand Binding Free 
Energies with Loop Closure and Experimental Constraints
Timothy J. Giese, Darrin M. York
J. Chem. Theory Comput. (2021)
DOI: 10.1021/acs.jctc.0c01219

[3] Extension of the Variational Free Energy Profile and Multistate Bennett 
Acceptance Ratio Methods for High-Dimensional Potential of Mean Force Profile 
Analysis
Timothy J. Giese, Şölen Ekesan, Darrin M. York
J. Phys. Chem. A (2021) 125, 4216-4232
DOI: 10.1021/acs.jpca.1c00736

[4] Multireference Generalization of the Weighted Thermodynamic
Perturbation Method
Timothy J. Giese, Jinzhe Zeng, and Darrin M. York
J. Phys. Chem. A (2022) 126, 8519-8533
DOI: 10.1021/acs.jpca.2c06201


The current snapshot correspond to my local development commits
- edgembar: Thu May 4 13:17:37 2023 -0400 commit 38cc0e86a6262b30b9795be8b667f4e6d720f0d9
- ndfes: Thu May 4 13:18:50 2023 -0400 commit e6586a7b3422aea80527cb6b3b5b7c8a8d303c6f
